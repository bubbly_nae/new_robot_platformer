using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventory : MonoBehaviour
{

    public List<int> Inv;
    public int invMaxSize = 10;

    public bool addItem(Pickup pickup)
    {
        if(Inv.Count < invMaxSize)
        {
            Inv.Add(pickup.getID());
            return true;
        }
        else
        {
            return false;
        }

    }

    public bool removeItem(int itemID)
    {
        if(Inv.Contains(itemID))
        {
            Inv.Remove(itemID);
            return true;
        }

        else
        {
            Debug.LogError("Item not in inventory list");
            return false;
        }
    }
}
