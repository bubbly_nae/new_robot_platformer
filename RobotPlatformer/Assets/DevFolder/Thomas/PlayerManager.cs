using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class PlayerManager : MonoBehaviour
{
    //State Control
    enum playerStates { IDLE, WALKING, RUNNING, JUMPING, SLIDING, WALLRUNENTER, WALLRUNNING, GRAPPLESHOT, GRAPPLEPULL, GRAPPLESWING, FREEFALL }
    public int curState = 0;
    int lastState;


    PlayerInput input;
    CharacterController cc;
    Rigidbody rb;
    public float h, j, v = 0f;
    public float speed = 10f;
    Vector3 moveVec;
    Vector2 axisInput;
    Vector2 gravityVec;
    float jumpVel = 10f;        //Base upward velocity when jumping
    float gravityAir = -4f;   //Downward acceleration when jumping/falling
    float gravityGround = -0.02f;    //Downward accleration when grounded, required to trigger isGrounded property

    float jumpApex;
    float jumpHeight = 2f;
    float jumpDur = 0.5f;
    float jumpFall = 2f;

    float turnFactor = 5f;

    bool isRunning = false;
    Camera cam;

    Vector3 grappleVector;
    Vector3 grapplePoint;
    float grappleRadius = 5f;
    
    //Animation and sound delegation
    public delegate void stateOutput(int s);

    public stateOutput onStateChange;
    bool grappleFlag = false;

   
    void Awake()
    {

        /*input = this.GetComponent<PlayerInput>();
        input.Direction = move;
<<<<<<< HEAD
        //input.Jump = jump;
        //input.Sprint = runToggle;
        //input.Grapple1 = grapple;
        //input.Grapple2 = grappleAlt;
        //input.Slide = slide;
=======
        input.JumpPress = jump;
        
        //input.SprintPress =;
        input.Grapple1 = grapple;
        input.Grapple2 = grappleAlt;
        input.SlidePress = slide;*/
>>>>>>> origin/thomas
        cam = Camera.main;
        cc = this.GetComponent<CharacterController>();
        rb = this.GetComponent<Rigidbody>();

        //Jump setup
        jumpApex = jumpDur / 2;
        gravityAir = (-2 * jumpHeight) / Mathf.Pow(jumpApex, 2);
        jumpVel = (2 * jumpHeight) / jumpApex;
        j = gravityGround;


        
    }

    

    void Update()
    {

        
        if(curState < 8)
        {
            rb.Sleep();
            moveAlways();
            gravity(curState);
            wallRunCheck();
        }
        else
        {
            /*if (Vector3.Distance(cc.transform.position, grapplePoint) > 0.1f)
            {
                cc.Move(grappleVector * Time.deltaTime * 5f) ;
            }
            else
            {
                switchState(0);  
            }*/

            rb.velocity = rb.velocity + moveVec * Time.deltaTime;
            rb.position = rb.position + moveVec * Time.deltaTime;

            rb.AddForce(moveVec * speed * Time.deltaTime, ForceMode.Impulse);
            if (!Mathf.Approximately(Vector3.Magnitude(rb.position - grapplePoint), grappleRadius))
            {
                rb.transform.position = (rb.transform.position - grapplePoint).normalized * grappleRadius + grapplePoint;

            }

            Vector3 upward = this.transform.position - grapplePoint;
            rb.transform.rotation = Quaternion.LookRotation(rb.transform.forward, upward);
        }

        
        
    }

    void LateUpdate()
    {
        if (grappleFlag)
            switchState(8);
        grappleFlag = false;

        
    }

    void switchState(int state)
    {
        if (state != curState)
        {
            lastState = curState;
            curState = state;
            onStateChange?.Invoke(state);
            if(lastState == 8)
            {
                cc.enabled = true;
                rb.Sleep();
            }
            if(state == 8)
            {
                cc.enabled = false;
                rb.WakeUp();
            }
        }

    }

    void move()
    {
        if (curState == 0)
            switchState(1);
    }
    void moveAlways()
    {
        if (cc.enabled)
        {
            
                
            h = Input.GetAxisRaw("Horizontal");
            v = Input.GetAxisRaw("Vertical");

            moveVec = new Vector3(h, j, v).normalized * Time.deltaTime;

            if(curState !=6)
                moveVec = Quaternion.Euler(0f, cam.transform.eulerAngles.y, 0f) * moveVec;
            else if (curState == 6)
            {
                moveVec = Quaternion.Euler(0f, cc.transform.forward.x, 0f) * moveVec;
            }
            axisInput = new Vector2(h, v).normalized;

            cc.Move(moveVec * speed);

            Vector2 motionCheck = new Vector2(h,v);
            
            if(motionCheck.normalized.magnitude < 0.1f)
            {
                switchState(0);
            }
            rotationCheck();
        }

    }

    void jump()
    {
      
        if(cc.isGrounded && curState !=3)
        {
            switchState(3);
            j = jumpVel;
          
        }
    }

    void gravity(int statecheck)
    {
        if (curState < 8)
        {
            if (cc.isGrounded && curState == 3)
            {
                j = gravityGround;
                if (lastState < 3)
                {
                    switchState(lastState);
                }
            }

            else if (!cc.isGrounded && !isWallRun)
            {
                float lastY = j;
                float newY = j + gravityAir * jumpFall * Time.deltaTime;
                float nextY = (lastY + newY) / 2;
                j = nextY;

            }
            else if(isWallRun)
            {
                float lastY = j;
                float newY = j + gravityGround * jumpFall * Time.deltaTime;
                float nextY = (lastY + newY) / 2;
                j = nextY;
            }
            else if (cc.isGrounded)
            {
                j = gravityGround;
            }    
            /*switch(statecheck)
            {
                case 3:
                    if (cc.isGrounded)
                    {
                        j = gravityGround;
                        if (lastState < 3)
                        {
                            switchState(lastState);
                        }

                        else if()
                        {
                                    //Work out what is breaking wall jumping
                        }
                    }


                    else if (!cc.isGrounded)
                    {
                        float lastY = j;
                        float newY = j + gravityAir * jumpFall * Time.deltaTime;
                        float nextY = (lastY + newY) / 2;
                        j = nextY;
                    }
                        break;

                default:
                    if (cc.isGrounded)
                        j = gravityGround;
                    else
                    {
                        float lastY = j;
                        float newY = j + gravityAir * jumpFall * Time.deltaTime;
                        float nextY = (lastY + newY) / 2;
                        j = nextY;
                    }
                    break;
            }*/
            gravityVec = new Vector2(0f, j).normalized;
            //cc.Move(gravityVec * Time.deltaTime);
        }
    }

    void rotationCheck()
    {
        Vector3 rotPos;
        rotPos.x = moveVec.x;
        rotPos.y = 0f;
        rotPos.z = moveVec.z;
        Quaternion currentRot = transform.rotation;

        if (axisInput.magnitude > 0.5f)
        {
            Quaternion target = Quaternion.LookRotation(rotPos);
            this.transform.rotation = Quaternion.Slerp(currentRot, target, turnFactor * Time.deltaTime);
        }
        
    }
    
    void runToggle()
    {
        if(curState == 1)
        {
            switchState(2);
            speed = 20f;
            isRunning = true;
        }
        else if(curState == 2)
        {
            switchState(1);
            speed = 10f;
            isRunning = false;
        }

        
    }

    void slide()
    {
        StartCoroutine(slideBoost());
        //Do animation related stuff here
    }

    IEnumerator slideBoost()
    {
        jumpVel = 20f;
        yield return new WaitForSeconds(0.5f);
        jumpVel = 10f;

    }
    void grapple()
    {

        
        Ray gRay = cam.ViewportPointToRay(new Vector3(0.5f,0.5f));
        RaycastHit hit;

        if (Physics.Raycast(gRay, out hit, 20))
        {
            
           grappleVector  = hit.point - this.transform.position;
    
            if(hit.collider.GetComponentInParent<Pickup>())
            {
                hit.collider.GetComponentInParent<Pickup>().grappled(this.transform);
            }

            else
            {
                
               
                grapplePoint = hit.point;
                grappleFlag = true;
                
                
            }
        }
    }

    void grappleAlt()
    {

    }

    //Wall Running
    public LayerMask wallMask;
    bool wallLeft, wallRight, isWallRun;
    void wallRunCheck()
    {

        wallLeft = Physics.Raycast(cc.transform.position, -cc.transform.right, 1f);
        
        wallRight = Physics.Raycast(cc.transform.position, cc.transform.right, 1f);
       
        if ((wallRight || wallLeft) && curState == 3)
          {
            wallRunStart();
            return;
          }
                
        if(curState == 6 && ((!wallRight && !wallLeft) || cc.isGrounded))
        {
            wallRunExit(cc.isGrounded);
        }
            
        
    }

    void wallRunStart()
    {
        j = gravityGround;
        isWallRun = true;
        switchState(6);

    }

    void wallRunExit(bool groundCheck)
    {
        isWallRun = false;
        if (groundCheck)
        {
            switchState(1);
            
        }
        else
        {
            switchState(3);
   
        }

    }
    
    void wallRunJump()
    {

    }
}
