using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public void mouseVisibile(bool state)
    {
        Cursor.visible = state;
    }

    public GameObject loadedMap;
    // Start is called before the first frame update
    void Start()
    {
        mouseVisibile(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
