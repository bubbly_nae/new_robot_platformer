using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Camera))]
public class CameraMovement : MonoBehaviour
{
    enum state { FIXED, MOVABLE};
    int curState = 1;
    public Transform focusTra;

    Vector3 offset;

    float xInput, yInput;
    float smoothFactor = 0.1f;

    float rotationSpeed = 2f;

    Vector3 newPos;
    RaycastHit camCollision;
    bool blocked = false;
    int playerLayer = 1 << 8;
    private void Awake()
    {
        offset = transform.position - focusTra.position;
        playerLayer = ~playerLayer;
    }

    private void Update()
    {
        blocked = Physics.Linecast( focusTra.position, offset, out camCollision, playerLayer);
        Debug.LogError(blocked);
        
    }
    private void LateUpdate()
    {
        if(curState == 1)
        {
            xInput = -Input.GetAxis("Mouse X") * rotationSpeed;
            yInput = Input.GetAxis("Mouse Y") * rotationSpeed;
            
            
            Quaternion qx = Quaternion.AngleAxis(xInput, Vector3.up);
            Quaternion qY = Quaternion.AngleAxis(yInput, this.transform.right);

            //Quaternion camAngle = Quaternion.AngleAxis(-Input.GetAxis("Mouse X") * rotationSpeed, Vector3.up);

            Quaternion camAngle = qx * qY;
            
            offset = camAngle * offset;
            
            /*if(Physics.SphereCast(this.transform.position, 0.2f, this.transform.forward, out camCollision, offset.magnitude-2f))
            {
                newPos = focusTra.position + (camAngle *  camCollision.point);
            }*/
             
            if(blocked)
                newPos = focusTra.position + (camAngle * camCollision.point);
            
            else newPos = focusTra.position + offset;
            transform.position = Vector3.Slerp(transform.position, newPos, smoothFactor);

            transform.LookAt(focusTra);

             
            


        }
        
    }

    void setState(int iState)
    {
        curState = iState;
    }
}
