using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Pickup : MonoBehaviour
{
    public int ID;
    bool grapple = false;
    Transform grappleDest;
    float grappleSpeed = 20f;
    public void pickUp(bool invCheck)
    {
        if(invCheck)
        {
            Destroy(gameObject);
        }

    }

    public void grappled(Transform target)
    {
        grapple = true;
        grappleDest = target;
    }

    private void Update()
    {
        if(grapple)
        {
            Vector3 moveVec = new Vector3(grappleDest.position.x - this.transform.position.x,grappleDest.position.y - this.transform.position.y, grappleDest.position.z - this.transform.position.z);
            this.transform.position = moveVec * grappleSpeed * Time.deltaTime;
        }
    }

    void OnTriggerEnter(Collider playerCheck)
    {
        if (playerCheck.CompareTag("Player"))
            pickUp(checkPInv(playerCheck));
    }


    bool checkPInv(Collider playerCheck)
    {
        if (playerCheck.GetComponent<Inventory>().addItem(this))
            return true;
        else return false;
    }

    public int getID()
    {
        return this.ID;
    }
}

[CreateAssetMenu(fileName = "new Module")]
public class VoiceModule : Pickup
{
   
    
   

    //Get sound stuff here


}
