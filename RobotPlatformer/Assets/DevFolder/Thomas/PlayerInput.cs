using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInput : MonoBehaviour
{

    public delegate void input();

    public input Direction;
    public input NoDirection;
    public input JumpPress;
    public input JumpRelease;
    public input Grapple1Press;
    public input Grapple1Release;
    public input Grapple2Press;
    public input Grapple2Release;
    public input SprintPress;
    public input SprintRelease;
    public input SlidePress;
    public input SlideRelease;

    void Update()
    {


        if ((Input.GetAxisRaw("Horizontal") != 0) || (Input.GetAxisRaw("Vertical") != 0))
        {
            Direction.Invoke();
        }
        if ((Input.GetAxisRaw("Horizontal") == 0) && (Input.GetAxisRaw("Vertical") == 0))
        {
            NoDirection.Invoke();
        }

        if (Input.GetButtonDown("Jump"))
        {
            JumpPress.Invoke();
        }

        if (Input.GetButtonUp("Jump"))
        {
            JumpRelease.Invoke();
        }

        if (Input.GetButtonDown("Fire1"))
<<<<<<< HEAD
        {
            Grapple1Press.Invoke();
        }
        if (Input.GetButtonUp("Fire1"))
        {
            Grapple1Release.Invoke();
        }
        if (Input.GetButtonDown("Fire2"))
        {
            Grapple2Press.Invoke();
        }
        if (Input.GetButtonUp("Fire2"))
        {
            Grapple2Release.Invoke();
        }
        if (Input.GetButtonDown("Fire3"))
        {
            SprintPress.Invoke();
        }
        if (Input.GetButtonUp("Fire3"))
        {
            SprintRelease.Invoke();
        }
        if (Input.GetButtonDown("Slide"))
        {
            SlidePress.Invoke();
=======
        {
            Grapple1Press.Invoke();
        }
        if (Input.GetButtonUp("Fire1"))
        {
            Grapple1Release.Invoke();
        }
        if (Input.GetButtonDown("Fire2"))
        {
            Grapple2Press.Invoke();
        }
        if (Input.GetButtonUp("Fire2"))
        {
            Grapple2Release.Invoke();
        }    
        if(Input.GetButtonDown("Fire3"))
        {
            SprintPress.Invoke();
        }
        if(Input.GetButtonUp("Fire3"))
        {
            SprintRelease.Invoke();
>>>>>>> origin/thomas
        }
        if (Input.GetButtonUp("Slide"))
        {
<<<<<<< HEAD
            //SlideRelease.Invoke();
=======
            SlidePress.Invoke();
        }
        if(Input.GetButtonUp("Slide"))
        {
            SlideRelease?.Invoke();
>>>>>>> origin/thomas
        }
    }
}
