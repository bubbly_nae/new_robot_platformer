using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
public class PlayerAnimation : MonoBehaviour
{
    int IDLEHASH,WALKHASH, RUNHASH, JUMPHASH;

    Animator pa;
    PlayerManager pm;

    
    private void Awake()
    {
        pa = this.GetComponent<Animator>();
       

        IDLEHASH = Animator.StringToHash("IDLE");
        WALKHASH = Animator.StringToHash("WALK");
        RUNHASH = Animator.StringToHash("RUN");
        JUMPHASH = Animator.StringToHash("JUMP");

        pm = this.GetComponent<PlayerManager>();
        pm.onStateChange = switchAnim;
    }


    void switchAnim(int state)
    {
        switch(state)
        {
            case 0:
                pa.SetTrigger(IDLEHASH);
                break;
            case 1:
                pa.SetTrigger(WALKHASH);
                break;
            case 2:
                pa.SetTrigger(RUNHASH);
                break;
            case 3:
                pa.SetTrigger(JUMPHASH);
                break;
            default:
                break;
        }
    }
}
