using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerState_Run : PlayerState_Base
{
    public PlayerState_Run(PlayerStateMachine currentContext, StateFactory playerStateFactory) : base(currentContext, playerStateFactory) { }
    public override void EnterState() 
    {
        ctx.pa.SetBool(ctx.RUNHASH, true);
    }
    public override void UpdateState() 
    {
        CheckSwitchState();
        ctx.groundMovement(true);
        ctx.handleRotation();
        
    }
    public override void ExitState() 
    {
        ctx.pa.SetBool(ctx.RUNHASH, false);
    }
    public override void CheckSwitchState() 
    {
        if (ctx.isMoving && !ctx.isRunning)
            SwitchState(fac.Walk());
        else if (!ctx.isMoving)
            SwitchState(fac.Idle());
        else if (ctx.isSliding)
            SwitchState(fac.Slide());
        else if (ctx.isGrappling)
            SwitchState(fac.Grapple());
    }
    public override void InitializeSubState() { }
}