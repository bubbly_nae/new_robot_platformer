

public class StateFactory
{
    PlayerStateMachine context;

    public StateFactory(PlayerStateMachine currentContext)
    {
        context = currentContext;
    }

    public PlayerState_Base Grounded() { return new PlayerState_Grounded(context, this); }

    public PlayerState_Base Idle() { return new PlayerState_Idle(context, this); }
                                                                 
    public PlayerState_Base Walk() { return new PlayerState_Walk(context, this); }

    public PlayerState_Base Run() { return new PlayerState_Run(context, this); }  
    
    public PlayerState_Base Slide() { return new PlayerState_Slide(context, this); }

    public PlayerState_Base Jump() { return new PlayerState_Jump(context, this); }

    public PlayerState_Base Freefall() { return new PlayerState_Freefall(context, this); }

    public PlayerState_Base Grapple() { return new PlayerState_Grapple(context, this); }

    public PlayerState_Base Grapple1Self() { return new PlayerState_Grapple1self(context, this); }

    public PlayerState_Base Grapple1Item() { return new PlayerState_Grapple1item(context, this); }

    public PlayerState_Base Grapple2() { return new PlayerState_Grapple2(context, this); }
}
