using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerState_Idle : PlayerState_Base
{
    public PlayerState_Idle(PlayerStateMachine currentContext, StateFactory playerStateFactory) : base(currentContext, playerStateFactory) { }
    public override void EnterState() 
    {
        ctx.pa.SetBool(ctx.IDLEHASH, true);
    }
    public override void UpdateState() 
    {
        CheckSwitchState();
    }
    public override void ExitState() 
    {
        ctx.pa.SetBool(ctx.IDLEHASH, false);
        
    }
    public override void CheckSwitchState() 
    {
        if (ctx.isMoving && ctx.isRunning)
        {
            SwitchState(fac.Run());
            ctx.pa.SetBool(ctx.IDLEHASH, false);
        }

        else if (ctx.isMoving)
        {
            ctx.pa.SetBool(ctx.IDLEHASH, false);
            SwitchState(fac.Walk());
        }
        else if (ctx.isGrappling)
        {
            ctx.pa.SetBool(ctx.IDLEHASH, false);
            SwitchState(fac.Grapple());
        }
            
        else if (ctx.isJumping)
        {
           
            SwitchState(fac.Jump());
        }

    }
    public override void InitializeSubState() { }
}