using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerState_Grounded : PlayerState_Base
{
    public PlayerState_Grounded(PlayerStateMachine currentContext, StateFactory playerStateFactory):base(currentContext,playerStateFactory)
    {
        isCoreState = true;
        InitializeSubState();
    } 
    public override void EnterState()
    {
        ctx.j = ctx.gravityGround;
    }
    public override void UpdateState() 
    {
        CheckSwitchState();
        
    }
    public override void ExitState() { }
    public override void CheckSwitchState()
    {
        if(ctx.jumpPressed)
        {
            SwitchState(fac.Jump());
            
        }
       
        else if(!ctx.cc.isGrounded && !ctx.jumpPressed)
        {
            SwitchState(fac.Freefall());
        }
    }
   public override void InitializeSubState() 
    { 
       if(!ctx.isMoving)
        {
            SetSubState(fac.Idle());
        }
        else if(ctx.isMoving && !ctx.isRunning)
        {
            SetSubState(fac.Walk());
        }
        else 
        {
            SetSubState(fac.Run());
        }
    }
}