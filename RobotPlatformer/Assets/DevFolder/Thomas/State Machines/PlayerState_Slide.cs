using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerState_Slide : PlayerState_Base
{
    public PlayerState_Slide(PlayerStateMachine currentContext, StateFactory playerStateFactory) : base(currentContext, playerStateFactory) 
    { 
    
    }
    public override void EnterState() 
    {
        ctx.Slide();
    }
    public override void UpdateState() 
    {
        CheckSwitchState();
    }
    public override void ExitState() 
    {
        ctx.endSlide();
    }
    public override void CheckSwitchState() 
    {
        if (ctx.jumpPressed)
        {
            SwitchState(fac.Jump());
        }
        else if(!ctx.isGrounded)
        {
            SwitchState(fac.Freefall());
        }
        else if(!ctx.isSliding)
        {
            SwitchState(fac.Run());
        }
    }
    public override void InitializeSubState() { }


}