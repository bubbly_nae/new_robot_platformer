using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerState_Grapple2 : PlayerState_Base
{
    public PlayerState_Grapple2(PlayerStateMachine currentContext, StateFactory playerStateFactory):base(currentContext,playerStateFactory)
    {
    } 
    public override void EnterState()
    {
        ctx.cc.enabled = false;
        ctx.rb.WakeUp();
    }
    public override void UpdateState() 
    {
        //CheckSwitchState();
        ctx.grappleMovement();
    }
    public override void ExitState()
    {
        ctx.cc.enabled = true;
        ctx.rb.Sleep();
    }
    public override void CheckSwitchState()
    {
        if (ctx.jumpPressed)
        {
            SwitchState(fac.Jump());
        }
    }
   public override void InitializeSubState() 
    { 
       
       
    }
}