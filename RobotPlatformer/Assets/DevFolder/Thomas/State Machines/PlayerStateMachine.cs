using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStateMachine : MonoBehaviour
{
    //References
    public PlayerInput input;
    public CharacterController cc;
    public Rigidbody rb;
    Camera cam;
    public Animator pa;
    //Movement variables
    public float h, j, v = 0f;
    public float speedWalk = 10f;
    public float speedRun = 20f;
    public float speedSwing = 15f;
    public Vector3 moveVec;
    Vector2 axisInput;
    Vector2 gravityVec;
    float turnFactor = 5f;

    //Jump Variables
    public float jumpVel = 10f;
    float jumpApex;
    float jumpHeight = 2f;
    float jumpDur = 0.5f;
    public float jumpFall = 2f;
    public float gravityGround = -0.02f;
    public float gravity = -4f;   //Downward acceleration when jumping/falling
   

    //Grapple Variables
    Vector3 grapplePoint;
    Vector3 grappleVector;
    public float maxLine = 5f;
    public float swingSpeed = 10f;


    //State control
    PlayerState_Base currentState;
    StateFactory states;
    public PlayerState_Base curState { get { return currentState; } set { currentState = value; } }

    [HideInInspector]
    public bool jumpPressed = false;
    [HideInInspector]
    public bool sprintPressed = false;
    [HideInInspector]
    public bool isGrounded = true;
    [HideInInspector]
    public bool isMoving = false;
    [HideInInspector]
    public bool isJumping = false;
    [HideInInspector]
    public bool isRunning = false;
    [HideInInspector]
    public bool isGrappling = false;
    [HideInInspector]
    public bool isSliding = false;
    [HideInInspector]
    public bool grapple1self = false;
    [HideInInspector]
    public bool grapple1item = false;
    [HideInInspector]
    public bool grapple2 = false;
    [HideInInspector]
    public bool canSlide = true;

<<<<<<< HEAD
=======

    //Animation control
    public int IDLEHASH = Animator.StringToHash("IDLE");
    public int WALKHASH = Animator.StringToHash("WALK");
    public int RUNHASH = Animator.StringToHash("RUN");
    public int JUMPHASH = Animator.StringToHash("JUMP");
>>>>>>> origin/thomas
    private void Awake()
    {

        input = this.GetComponent<PlayerInput>();
        input.Direction = move;
        input.NoDirection = noMove;
        input.JumpPress = jumpPress;
        input.JumpRelease = jumpRelease;
        input.SprintPress = sprintPress;
        input.SprintRelease = sprintRelease;
        input.Grapple1Press = grapple1Press;
        input.Grapple1Release = grapple1Release;
        input.Grapple2Press = grapple2Press;
        input.Grapple2Release = grapple2Release;
        input.SlidePress = Slide;
        

        cam = Camera.main;
        cc = this.GetComponent<CharacterController>();
        rb = this.GetComponent<Rigidbody>();
        pa = this.GetComponent<Animator>();

        //Jump setup
        jumpApex = jumpDur / 2;
        gravity = (-2 * jumpHeight) / Mathf.Pow(jumpApex, 2);
        jumpVel = (2 * jumpHeight) / jumpApex;
        j = gravityGround;

        states = new StateFactory(this);
        currentState = states.Grounded();
        currentState.EnterState();

        
    }

    private void Start()
    {
        cc.Move(new Vector3(0, gravity) * Time.deltaTime);
    }
    private void Update()
    {
        if (!isGrappling)
            rb.Sleep();
        currentState.UpdateStates();
        //Debug.Log(currentState);
        
    }

    private void LateUpdate()
    {
        isGrounded = cc.isGrounded;
    }
    public void move()
    {
        isMoving = true;
    }
    public void noMove()
    {
        isMoving = false;
    }

    public void sprintPress()
    {
        sprintPressed = true;
        isRunning = true;
    }

    public void sprintRelease()
    {
        sprintPressed = false;
        isRunning = false;
    }

    public void jumpPress()
    {
        jumpPressed = true;
        if(cc.isGrounded)
        {
            isJumping = true;
        }
    }
    
    public void jumpRelease()
    {
        jumpPressed = false;
        if(cc.isGrounded)
        {
            isJumping = false;
        }
    }

    public void grapple1Press()
    {
        Ray gRay = cam.ViewportPointToRay(new Vector3(0.5f, 0.5f));
        RaycastHit hit;

        if (Physics.Raycast(gRay, out hit, 20))
        {

            grappleVector = hit.point - this.transform.position;

            if (hit.collider.GetComponentInParent<Pickup>())
            {
                hit.collider.GetComponentInParent<Pickup>().grappled(this.transform);
                grapple1item = true;
                isGrappling = true;
            }

            else
            {

                
                grapplePoint = hit.point;
                grapple1self = true;
                isGrappling = true;

            }
        }

        
    }

    public void grapple1Release()
    {
        
    }

    public void breakGrapple1Self()
    {
        grapple1self = false;
       
    }

    public void breakGrapple1Item()
    {
        grapple1item = false;
        
    }
    public void grapple2Press()
    {
        Ray gRay = cam.ViewportPointToRay(new Vector3(0.5f, 0.5f));
        RaycastHit hit;

        if (Physics.Raycast(gRay, out hit, 20))
        {
            grapplePoint = hit.point;
            grapple2 = true;
            isGrappling = true;
        }
    }
    public void grapple2Release()
    {

    }


    public void breakGrapple2()
    {
        grapple2 = false;
    }

    public void groundMovement(bool running)
    {
        float usedSpeed;
        h = Input.GetAxisRaw("Horizontal");
        v = Input.GetAxisRaw("Vertical");
        if (running)
        {
            usedSpeed = speedRun;
        }
        else
        {
            usedSpeed = speedWalk;
        }

        moveVec = new Vector3(h, j, v).normalized;
        moveVec = new Vector3(moveVec.x * usedSpeed, j, moveVec.z * usedSpeed) * Time.deltaTime;
        moveVec = Quaternion.Euler(0f, cam.transform.eulerAngles.y, 0f) * moveVec;

        cc.Move(moveVec);
    }
    
    public void handleRotation()
    {
        Vector3 rotPos;
        rotPos.x = moveVec.x;
        rotPos.y = 0f;
        rotPos.z = moveVec.z;
        Quaternion currentRot = transform.rotation;

        if (isMoving)
        {
            Quaternion target = Quaternion.LookRotation(rotPos);
            this.transform.rotation = Quaternion.Slerp(currentRot, target, turnFactor * Time.deltaTime);
        }
    }

    public void grappleMovement()
    {
        h = Input.GetAxisRaw("Horizontal") * speedSwing;
        v = Input.GetAxisRaw("Vertical") * speedSwing;
        moveVec = new Vector3(h, 0f, v);
        rb.velocity = rb.velocity + moveVec * Time.deltaTime;
        rb.position = rb.position + moveVec * Time.deltaTime;

        rb.AddForce(moveVec * speedSwing * Time.deltaTime, ForceMode.Impulse);
        if (!Mathf.Approximately(Vector3.Magnitude(rb.position - grapplePoint), maxLine))
        {
            rb.transform.position = (rb.transform.position - grapplePoint).normalized * maxLine + grapplePoint;

        }

        Vector3 upward = this.transform.position - grapplePoint;
        rb.transform.rotation = Quaternion.LookRotation(rb.transform.forward, upward);
    }

    public void Slide()
    {
        if(canSlide)
            StartCoroutine(slideBoost());

    }

    public void endSlide()
    {
        StopCoroutine(slideBoost());
        StartCoroutine(slideDelay());
        isSliding = false;
    }
    IEnumerator slideBoost()
    {
        canSlide = false;
        isSliding = true;
        jumpVel = 20f;
        yield return new WaitForSeconds(1f);
        jumpVel = 10f;
        isSliding = false;
        endSlide();
    }

    IEnumerator slideDelay()
    {
        yield return new WaitForSeconds(0.5f);
        canSlide = true;
    }
}
