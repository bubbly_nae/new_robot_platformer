using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerState_Swing : PlayerState_Base
{
    public PlayerState_Swing(PlayerStateMachine currentContext, StateFactory playerStateFactory) : base(currentContext, playerStateFactory) { }
    public override void EnterState() 
    {
        ctx.isJumping = false;
        ctx.isGrappling = true;

        ctx.cc.enabled = false;
        ctx.rb.WakeUp();
    }
    public override void UpdateState()
    {
        CheckSwitchState();
        ctx.groundMovement(true);
    }
    public override void ExitState() 
    {
        ctx.isGrappling = false;
        ctx.rb.Sleep();
        ctx.cc.enabled = true;
    }
    public override void CheckSwitchState()
    {
        if (ctx.isJumping)
        {
            SwitchState(fac.Jump());
        }
    }
    public override void InitializeSubState() { }
}
