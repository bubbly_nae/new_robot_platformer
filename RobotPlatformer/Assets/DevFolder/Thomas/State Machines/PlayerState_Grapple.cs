using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerState_Grapple : PlayerState_Base
{
    public PlayerState_Grapple(PlayerStateMachine currentContext, StateFactory playerStateFactory):base(currentContext,playerStateFactory)
    {
        isCoreState = true;
        InitializeSubState();
    } 
    public override void EnterState()
    {
        
    }
    public override void UpdateState() 
    {
        CheckSwitchState();
        
    }
    public override void ExitState()
    {
        ctx.isGrappling = false;
    }
    public override void CheckSwitchState()
    {
        if(ctx.jumpPressed)
        {
            SwitchState(fac.Jump());
            
        }
       
        else if(!ctx.cc.isGrounded && !ctx.jumpPressed)
        {
            SwitchState(fac.Freefall());
        }
    }
   public override void InitializeSubState() 
    { 
       if(ctx.grapple1item)
        {
            SetSubState(fac.Grapple1Item());
        }
        else if (ctx.grapple1self)
        {
            SetSubState(fac.Grapple1Self());
        }
        else 
        {
            SetSubState(fac.Grapple2());
        }
       
    }
}