using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class PlayerState_Base
{

    protected bool isCoreState = false;  

    protected PlayerStateMachine ctx;
    protected StateFactory fac;

    protected PlayerState_Base curSuperState;
    protected PlayerState_Base curSubState;
    public PlayerState_Base(PlayerStateMachine currentContext, StateFactory playerStateFactory)
    {
        ctx = currentContext;
        fac = playerStateFactory;
    }

    public abstract void EnterState();
    public abstract void UpdateState();
    public abstract void ExitState();
    public abstract void CheckSwitchState();
    public abstract void InitializeSubState();

        
    public void UpdateStates() 
    {
        UpdateState();
        if(curSubState != null)
        {
            curSubState.UpdateStates();
        }
        Debug.Log(curSubState);
    }

    protected void SwitchState(PlayerState_Base newState)
    {
        ExitState();
        newState.EnterState();
        if (isCoreState)
        {
            ctx.curState = newState;
        }
        else if (curSuperState != null)
        {
            curSuperState.SetSubState(newState);
        }
    }

    protected void SetSuperState(PlayerState_Base newSuperState) 
    {
        curSuperState = newSuperState;
    }

    protected void SetSubState(PlayerState_Base newSubState) 
    {
        curSubState = newSubState;
        newSubState.SetSuperState(this);
    }

   
}
