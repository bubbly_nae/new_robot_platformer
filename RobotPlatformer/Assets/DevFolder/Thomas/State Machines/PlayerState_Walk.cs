using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerState_Walk : PlayerState_Base
{
    public PlayerState_Walk(PlayerStateMachine currentContext, StateFactory playerStateFactory) : base(currentContext, playerStateFactory) { }
    public override void EnterState() 
    {
        ctx.pa.SetBool(ctx.WALKHASH, true);
    }
    public override void UpdateState()
    {
        CheckSwitchState();
        ctx.groundMovement(false);
        ctx.handleRotation();
        
    }
    public override void ExitState() 
    {
        ctx.pa.SetBool(ctx.WALKHASH, false);
    }
    public override void CheckSwitchState() 
    {
        if (!ctx.isMoving)

            SwitchState(fac.Idle());


        else if (ctx.isMoving && ctx.isRunning)
        {
            SwitchState(fac.Run());
            
        }
            
        else if (ctx.isGrappling)
            SwitchState(fac.Grapple());
    }
    public override void InitializeSubState() { }

    
}
