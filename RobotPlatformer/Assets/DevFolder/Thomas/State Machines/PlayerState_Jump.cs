using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerState_Jump : PlayerState_Base
{
    public PlayerState_Jump(PlayerStateMachine currentContext, StateFactory playerStateFactory) : base(currentContext, playerStateFactory)
    {
        isCoreState = true;
    }
    public override void EnterState() 
    {
        ctx.j = ctx.jumpVel;
        ctx.pa.SetBool(ctx.JUMPHASH, true);
        ctx.isJumping = true;
        if(ctx.jumpVel > 10f)
        {
            ctx.jumpVel = 10f;
        }
    }
    public override void UpdateState() 
    {
        CheckSwitchState();
        ctx.groundMovement(ctx.isRunning);
        ctx.handleRotation();
        handleGravity();
    }
    public override void ExitState() 
    {
        ctx.isJumping = false;
        ctx.pa.SetBool(ctx.JUMPHASH, false);
    }
    public override void CheckSwitchState() 
    {

        if(ctx.isGrounded)
        {
            SwitchState(fac.Grounded());
        }
    }
    public override void InitializeSubState() { }


    void handleGravity()
    {
            float lastY = ctx.j;
            float newY = ctx.j + ctx.gravity * ctx.jumpFall * Time.deltaTime;
            float nextY = (lastY + newY) / 2;
            ctx.j = ctx.j = Mathf.Max(nextY, -20f);
    }
}