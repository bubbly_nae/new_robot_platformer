using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerState_Freefall : PlayerState_Base
{
    public PlayerState_Freefall(PlayerStateMachine currentContext, StateFactory playerStateFactory) : base(currentContext, playerStateFactory)
    {
        isCoreState = true;
    }
    public override void EnterState() 
    {
        ctx.j = ctx.gravity;
        if (ctx.jumpVel > 10f)
        {
            ctx.jumpVel = 10f;
        }
    }
    public override void UpdateState() 
    {
        CheckSwitchState();
        ctx.groundMovement(ctx.isRunning);
        ctx.handleRotation();
        handleGravity();
    }
    public override void ExitState() 
    {
        
    }
    public override void CheckSwitchState() 
    {
        if(ctx.cc.isGrounded)
        {
            SwitchState(fac.Grounded());
        }
        
    }
    public override void InitializeSubState() { }


    void handleGravity()
    {
            float lastY = ctx.j;
            float newY = ctx.j + ctx.gravity * ctx.jumpFall * Time.deltaTime;
            float nextY = (lastY + newY) / 2;
            ctx.j = Mathf.Max (nextY, -20f);
    }
}